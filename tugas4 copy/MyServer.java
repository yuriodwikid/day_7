package Comp.tugas4;

import java.io.*;
import java.net.*;
import java.util.*;

public class MyServer {
    static String inputPort;

    public static void main(String[] args) {
        final ServerSocket serverSocket;
        final Socket clientSocket;
        final BufferedReader in;
        final PrintWriter out;
        final Scanner input = new Scanner(System.in);

        try {
            try (InputStream inp = new FileInputStream("/Users/ada-nb185/Documents/config.txt")) {

                Properties prop = new Properties();

                prop.load(inp);

                inputPort = prop.getProperty("port");
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            serverSocket = new ServerSocket(Integer.parseInt(inputPort));
            clientSocket = serverSocket.accept();

            out = new PrintWriter(clientSocket.getOutputStream());
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            Thread sender = new Thread(new Runnable() {
                String msg;
                @Override
                public void run() {
                    while (true) {
                        msg = input.nextLine();
                        out.println(msg);
                        out.flush();
                    }
                }
            });
            sender.start();

            Thread receive = new Thread(new Runnable() {
                String msg;
                @Override
                public void run() {
                    try {
                        msg = in.readLine();

                        while(msg != null) {
                            if (msg.equals("exit")) {
                                break;
                            }
                            System.out.println("Client: "+msg);
                            msg = in.readLine();
                        }

                        System.out.println("Client cabut bro");
                        out.close();
                        clientSocket.close();
                        serverSocket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            receive.start();
        } catch(Exception e) {
            System.out.println(e);
        }
    }
}
