package Comp.tugas3syulit;

import java.util.ArrayList;

public class Siswa {
    int idSiswa;
    String nama;
    ArrayList<Double> arrSis = new ArrayList<Double>();

    public Siswa(int idSiswa, String nama, ArrayList<Double> arrSis) {
        this.idSiswa = idSiswa;
        this.nama = nama;
        this.arrSis = arrSis;
    }

    public int getidSiswa() {
        return idSiswa;
    }

    public void setidSiswa(int idSiswa) {
        this.idSiswa = idSiswa;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public ArrayList<Double> getAlmhs() {
        return arrSis;
    }

    public void setAlmhs(ArrayList<Double> arrSis) {
        this.arrSis = arrSis;
    }
}



