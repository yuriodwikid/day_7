package Comp.tugas3syulit;

import java.util.ArrayList;
import java.util.Collections;

public class Multi extends Thread{
    private ArrayList <Siswa> dataSiswa = new ArrayList<>();
    public Multi(ArrayList<Siswa>dataSiswa){
        this.dataSiswa = dataSiswa;
    }

    public void run(){
        Collections.sort(dataSiswa, new Sorting());
        System.out.printf("%-5s %-5s %-10s %-10s %-10s %n","ID","Nama","Nilai Bing","Nilai Fisika","Nilai Algoritma");
        for (Siswa s : dataSiswa) {
            System.out.printf("%-5s %-5s %-10s %-10s %-10s %n", s.getidSiswa(),s.getNama(),s.getAlmhs().get(0),s.getAlmhs().get(1),s.getAlmhs().get(2));
        }
    }
}
