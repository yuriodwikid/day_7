package Comp.tugas3syulit;

import Comp.tugas3syulit.Siswa;

import java.util.Collections;
import java.io.*;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.Comparator;

class Sorting implements Comparator <Siswa> {
    public int compare(Siswa x, Siswa y) {
        return x.getidSiswa() - y.getidSiswa();
    }
}
public class Main {
    public static void main(String[] args) throws Exception {
        Scanner scan = new Scanner(System.in);
        ArrayList<Siswa> dataSiswa = new ArrayList<Siswa>();
        int nomor;
        System.out.println("Input username (e-mail) : ");
        String email = scan.nextLine();
        System.out.println("Password : ");
        String password = scan.nextLine();

        boolean mail = Pattern.matches("^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@" + "[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$", email);
        boolean pass = Pattern.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,20}$", password);
        if (mail == true && pass == true) {
            try (InputStream input = new FileInputStream("/Users/ada-nb185/Documents/credential.txt")) {
                Properties prop = new Properties();
                prop.load(input);
                if (email.equals(prop.getProperty("username")) && (password.equals(prop.getProperty("password")))) {
                    System.out.println("Anda berhasil login");
                    do {
                        System.out.println("MENU");
                        System.out.println("-------------");
                        System.out.println("1.Create & input data mahasiswa");
                        System.out.println("2.Tampilkan laporan nilai data mahasiswa");
                        System.out.println("3.Tampilkan di layar & tulis ke file");
                        System.out.println("4.EXIT");
                        System.out.println("Input nomor : ");
                        nomor = scan.nextInt();

                        switch (nomor) {
                            case 1:
                                System.out.println("-----Create & input data mahasiswa-----");
                                System.out.println("Membuat berapa data mahasiswa : ");
                                int akun = scan.nextInt();
                                for (int i = 0; i < akun; i++) {
                                    System.out.println("Input ID : ");
                                    int id = scan.nextInt();
                                    System.out.println("Input Nama :");
                                    String nama = scan.next();
                                    System.out.println("Input Nilai Bahasa Inggris :");
                                    double bing = scan.nextDouble();
                                    System.out.println("Input Nilai Fisika :");
                                    double fisika = scan.nextDouble();
                                    System.out.println("Input Nilai Algoritma :");
                                    double algo = scan.nextDouble();
                                    ArrayList<Double> almhs = new ArrayList<Double>();
                                    almhs.add(bing);
                                    almhs.add(fisika);
                                    almhs.add(algo);
                                    dataSiswa.add(new Siswa(id, nama, almhs));
                                }
                                System.out.println("Data berhasil di-input");
                                break;
                            case 2:
                                System.out.println("Laporan Data Siswa");
                                Collections.sort(dataSiswa, new Sorting());
                                System.out.printf("%-5s %-5s %-10s %-10s %-10s %n","ID","Nama","Nilai Bing","Nilai Fisika","Nilai Algoritma");
                                for (Siswa s : dataSiswa) {
                                    System.out.printf("%-5s %-5s %-10s %-10s %-10s %n", s.getidSiswa(),s.getNama(),s.getAlmhs().get(0),s.getAlmhs().get(1),s.getAlmhs().get(2));
                                }
                                break;
                            case 3:
                                System.out.println("Tampil di Layar");
                                Multi t1 = new Multi(dataSiswa);
                                t1.start();
                                Multi2 t2 = new Multi2(dataSiswa);
                                t2.start();
                                break;
                            default:
                                if (nomor == 4) {
                                    break;
                                } else {
                                    System.out.println("Menu tidak ada");
                                }
                        }
                    } while (nomor != 4);
                    System.out.println("EXIT");
                } else {
                    System.out.println("Maaf login tidak berhasil");
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } else {
            System.out.println("Email dan password kurang tepat!");
        }
    }
}




